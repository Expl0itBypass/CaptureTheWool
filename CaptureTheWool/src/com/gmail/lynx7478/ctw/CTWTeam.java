package com.gmail.lynx7478.ctw;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import com.gmail.lynx7478.ctw.utils.ScoreboardUtils;

public class CTWTeam {
	
	public static final CTWTeam RGB = new CTWTeam("RGB", ChatColor.RED);
	public static final CTWTeam CMY = new CTWTeam("CMY", ChatColor.AQUA);
	
	private String name;
	private ChatColor color;
	private Team team;
	
	public CTWTeam(String name, ChatColor color)
	{
		this.name = name;
		this.color = color;
		this.team = ScoreboardUtils.scoreboard.registerNewTeam(name);
		team.setAllowFriendlyFire(false);
		team.setCanSeeFriendlyInvisibles(true);
		team.setPrefix(this.color.toString());
	}
	
	public void joinTeam(Player p)
	{
		this.team.addPlayer(p);
	}
}