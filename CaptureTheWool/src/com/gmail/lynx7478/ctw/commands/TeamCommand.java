package com.gmail.lynx7478.ctw.commands;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.lynx7478.ctw.CTW;
import com.gmail.lynx7478.ctw.CTWPlayer;
import com.gmail.lynx7478.ctw.CTWTeam;

public class TeamCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command team, String s, String[] args) {
		if(team.getName().equalsIgnoreCase("team"))
		{
			if(!(sender instanceof Player)){
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Only players can execute this command!");
				return false;
			}
			Player p = (Player) sender;
			CTWPlayer ctw = CTWPlayer.get(p.getUniqueId());
			if(ctw.getTeam() == null)
			{
				if(args[0].equalsIgnoreCase("RGB"))
				{
					ctw.setTeam(CTWTeam.RGB);
					p.sendMessage(ChatColor.DARK_PURPLE + "You have joined the " + ChatColor.RED + "R" + ChatColor.GREEN + "G" + ChatColor.BLUE + "B" + ChatColor.DARK_PURPLE + " team!");
				}else if(args[0].equalsIgnoreCase("CMY"))
				{
					ctw.setTeam(CTWTeam.CMY);
					p.sendMessage(ChatColor.DARK_PURPLE + "You have joined the " + ChatColor.AQUA + "C" + ChatColor.LIGHT_PURPLE + "M" + ChatColor.YELLOW + "Y" + ChatColor.DARK_PURPLE + " team!");
				}
			}
		}
		return false;
	}

}
