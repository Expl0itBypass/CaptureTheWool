package com.gmail.lynx7478.ctw.utils;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardUtils {
	
	
	public static final Scoreboard scoreboard;
	private static final Objective obj;
	static
	{
		scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		obj = scoreboard.registerNewObjective("CAT", "MEOW MEOW");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	}

}
