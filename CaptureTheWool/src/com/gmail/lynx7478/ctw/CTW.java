package com.gmail.lynx7478.ctw;

import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.lynx7478.ctw.commands.*;
import com.gmail.lynx7478.ctw.managers.*;
import com.gmail.lynx7478.ctw.utils.ScoreboardUtils;

public class CTW extends JavaPlugin {
	
	private static CTW instance;
	
	public static CTW getInstance()
	{
		return instance;
	}
	public ScoreboardUtils scoreboard;
	
	@Override
	public void onEnable()
	{
		instance = this;
		init();
	}
	
	private void init()
	{
		scoreboard = new ScoreboardUtils();
		loadListeners();
		loadCommands();
		setupConfig();
	}
	
	private void loadListeners()
	{
		this.getServer().getPluginManager().registerEvents(new JoinManager(), this);
	}
	
	private void loadCommands()
	{
		this.getCommand("team").setExecutor(new TeamCommand());
	}
	
	private void setupConfig()
	{
		
	}
}