package com.gmail.lynx7478.ctw;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

public class CTWPlayer {
	
	private static HashMap<UUID, CTWPlayer> players = new HashMap<UUID, CTWPlayer>();
	
	public static CTWPlayer get(UUID id)
	{
		return players.get(id);
	}
	
	private Player p;
	private UUID id;
	private CTWTeam team;
	// private KitBase kit;
	
	@SuppressWarnings("static-access")
	public CTWPlayer(Player p)
	{
		this.p = p;
		this.id = p.getUniqueId();
		this.players.put(this.id, this);
		team = null;
	}
	
	public void setTeam(CTWTeam team)
	{
		this.team = team;
		team.joinTeam(this.p);
	}
	
	public CTWTeam getTeam()
	{
		return team;
	}
	
	public UUID getID()
	{
		return id;
	}
	
	public Player getBukkitPlayer()
	{
		return p;
	}

}
