package com.gmail.lynx7478.ctw.managers;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.gmail.lynx7478.ctw.CTWPlayer;

public class JoinManager implements Listener {
	
	@SuppressWarnings("unused")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		Player p = e.getPlayer();
		CTWPlayer ctw = new CTWPlayer(p);
	}

}
